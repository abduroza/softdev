<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Person;
use App\Events\SearchEvent;

class PersonController extends Controller
{
    //live search
    public function search(Request $request)
    {
        $query = $request->query('query');
        $products = Person::where('first_name', 'like', '%' . $query . '%')
            ->orWhere('last_name', 'like', '%' . $query . '%')
            ->orWhere('language', 'like', '%' . $query . '%')
            ->orWhere('upload', 'like', '%' . $query . '%')
            ->get();

        //broadcast search results with Pusher channels
        event(new SearchEvent($products));

        return response()->json("ok");
    }
    // mengambil semua data
    public function all()
    {
        return Person::all();
    }

    // mengambil data by id
    public function show($id)
    {
        return Person::find($id);
    }

    // menambah data
    public function store(Request $request)
    {
        $person = Person::create($request->all());
        if($request->hasFile('upload')){
            $request->file('upload')->move('uploads', $request->file('upload')->getClientOriginalName());
            $person->upload = $request->file('upload')->getClientOriginalName();
        }
        $person->save();

        return $person;
    }

    // mengubah data
    public function update(Request $request, $id)
    {
        $person = Person::find($id);
        $person->update($request->all());
        if($request->hasFile('upload')){
            $request->file('upload')->move('uploads', $request->file('upload')->getClientOriginalName());
            $person->upload = $request->file('upload')->getClientOriginalName();

        }
        $person->save();
        return $person;
    }

    // menghapus data
    public function delete($id)
    {
        $person = Person::find($id);
        $person->delete();
        return 204;
    }
}
