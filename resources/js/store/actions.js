let actions = {
    SEARCH_PERSONS({ commit }, query) {
        let params = {
            query
        };
        axios
            .get(`/api/search`, { params })
            .then(res => {
                if (res.data === "ok") console.log("request sent successfully");
            })
            .catch(err => {
                console.log(err);
            });
    },
    GET_PERSONS({ commit }) {
        axios
            .get("/api/person")
            .then(res => {
                {
                    commit("SET_PERSONS", res.data);
                }
            })
            .catch(err => {
                console.log(err);
            });
    }
};

export default actions;
